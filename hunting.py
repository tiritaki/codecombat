
points = []
points[0] = {"x": 89, "y": 48}
points[1] = {"x": 64, "y": 66}
points[2] = {"x": 62, "y": 29}


friends = hero.findFriends()
for j in range(3):
    point = points[j]
    friend = friends[j]
    enemy = friend.findNearestEnemy()
    if enemy:
        # Command friend to attack.
        hero.command(friend, "attack", enemy)
    else:
        # Command friend to move to point.
        hero.command(friend, "move", point)
