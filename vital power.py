# If the peasant is damaged, the flowers will shrink!

def pickUpNearestCoin():
    items = hero.findItems()
    nearestCoin = hero.findNearest(items)
    if nearestCoin:
        hero.move(nearestCoin.pos)

# This function has your hero summon a soldier.
def summonSoldiers():
    # Fill in code here to summon a soldier if you have enough gold.
    if hero.gold > hero.costOf("soldier"):
        hero.summon("soldier")
    pass


# This function commands your soldiers to attack their nearest enemy.
def commandSoldiers():
    for soldier in hero.findByType("soldier"):
        enemy = soldier.findNearestEnemy()
        if enemy:
            hero.command(soldier, "attack", enemy)


peasant = hero.findByType("peasant")[0]

while True:
    summonSoldiers()
    commandSoldiers()
    pickUpNearestCoin()
