while True:
    # Move to the nearest coin.
    # Use move instead of moveXY so you can command constantly.
    coin = hero.findNearest(hero.findItems())
    if coin:
        hero.move(coin.pos)
    # hero.say("I need coins!")
    # If you have funds for a soldier, summon one.
    if hero.gold > hero.costOf("soldier"):
        # hero.say("I should summon something here!")
        hero.summon("soldier")
    enemy = hero.findNearest(hero.findEnemies())
    if enemy:
        # Loop over all your soldiers and order them to attack.
        soldiers = hero.findFriends()
        soldierIndex = 0
        while soldierIndex < len(soldiers):
            soldier = soldiers[soldierIndex]
            hero.command(soldier, "attack", enemy)
            soldierIndex += 1
            # Use the 'attack' command to make your soldiers attack.

